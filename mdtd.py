#!/usr/bin/env python3

import argparse, sys, re


#####
# parse arguments
#####
argp = argparse.ArgumentParser(description='mdtd to todo.txt: Convert mdtd-formatted Markdown to todo.txt format')
argp.add_argument('outtype', help="Output type; either 'td' or 'org'")
argp.add_argument('infile', nargs='?', type=argparse.FileType('r'), default=sys.stdin, help='A file in mdtd format')
args = argp.parse_args()

#####
# helper functions
#####

def orgmode_make_tags_string(projects_and_contexts_list):
    tag_strings_list = [item[1:] for item in projects_and_contexts_list]
    if len(tag_strings_list):
        tags_string = ' :' + ':'.join(tag_strings_list) + ':'
    else:
        tags_string = ''
    return tags_string

flatten = lambda l: [x for l2 in l for x in l2]


#####
# initialize variables
#####

current_implicit_contexts_per_section_level = [[], [], [], [], [], [], []]
current_implicit_projects_per_section_level = [[], [], [], [], [], [], []]
current_implicit_suffix = ''

currently_in_todo_item_notes = False

currently_awaiting_first_todo_item_note = False

sectionlevel = 0

previous_line_was_blank_line = False


#####
# process the input file line-by-line
#####

for line in args.infile.readlines():

    #    
    # section header
    #
    m = re.match(r'\s*(#+)( .*)?', line)
    if m:
        if m.group(2):
            heading = m.group(2)
        else:
            heading = ''
        
        if currently_in_todo_item_notes:
            if args.outtype == 'td':
                print()
            currently_in_todo_item_notes = False
        previous_line_was_blank_line = False
        
        sectionlevel = len(m.group(1))
            
        contexts = re.findall(r'@\S+', line)
        projects = re.findall(r'\+\S+', line)

        # compute implicit contexts and projects
        for sectionlevel_to_reset in range(sectionlevel, 7):
            current_implicit_contexts_per_section_level[sectionlevel_to_reset] = []
            current_implicit_projects_per_section_level[sectionlevel_to_reset] = []
        
        for context in contexts:
            current_implicit_contexts_per_section_level[sectionlevel].append(context)
 
        for project in projects:
            current_implicit_projects_per_section_level[sectionlevel].append(project)
            
        current_implicit_contexts = flatten(current_implicit_contexts_per_section_level)
        current_implicit_projects = flatten(current_implicit_projects_per_section_level)
        if   args.outtype == 'td':
            current_implicit_suffix = ' '.join(list(set(current_implicit_projects + current_implicit_contexts)))
            if len(current_implicit_suffix) > 0:
                current_implicit_suffix = ' ' + current_implicit_suffix

        # output
        if   args.outtype == 'td':
            # compute parent contexts and projects (same as above, but the ones added on this line)
            parent_implicit_contexts = flatten(current_implicit_contexts_per_section_level[:sectionlevel])
            parent_implicit_projects = flatten(current_implicit_projects_per_section_level[:sectionlevel])
            parent_implicit_suffix = ' '.join(list(set(parent_implicit_projects + parent_implicit_contexts)))
            if len(parent_implicit_suffix) > 0:
                parent_implicit_suffix = ' ' + parent_implicit_suffix
            
            print(('  ' * (sectionlevel-1)) + ('#'*sectionlevel) + heading + parent_implicit_suffix)
            
        elif args.outtype == 'org':
            output_line = ('*'*sectionlevel) + heading
            print(output_line + orgmode_make_tags_string(projects + contexts))
            
            
        continue

    
    #    
    # horizontal rule
    #    
    m = re.match(r'---*\s*', line)
    if m:
        if currently_in_todo_item_notes:
            if args.outtype == 'td':
                print()
            currently_in_todo_item_notes = False
        previous_line_was_blank_line = False
            
        if  args.outtype == 'td':
            print(line[:-1])
        elif args.outtype == 'org':
            print()
        continue

    #    
    # task
    #    
    m = re.match(r'(\s+)*-\s*(.*)', line)
    if m:
        if currently_in_todo_item_notes:
            if args.outtype == 'td':
                print()
            currently_in_todo_item_notes = False
        previous_line_was_blank_line = False

        contexts = re.findall(r'@\S+', line)
        projects = re.findall(r'\+\S+', line)
        
        currently_in_todo_item_notes = True
        currently_awaiting_first_todo_item_note = True

        if m.group(1):
            indentation = re.sub('\t', r'    ', m.group(1)) # tabs to spaces
        else:
            indentation = ''
        indentationlevel = int(len(indentation)/4)
        
        # output
        if   args.outtype == 'td':
            print(('  ' * sectionlevel) + indentation + m.group(2) + current_implicit_suffix, end = '')  # note: unsectioned and section 0 indentation are the same
        elif args.outtype == 'org':
            print(('*' * indentationlevel) + ('*' * (sectionlevel+1)) + ' ' + m.group(2) + orgmode_make_tags_string(projects + contexts))
            
        continue

    #    
    # task note
    #    
    if currently_in_todo_item_notes:
        m = re.match(r'  \s*(.+)', line)
        if m:
            previous_line_was_blank_line = False
            
            if currently_awaiting_first_todo_item_note:
                if args.outtype == 'td':
                    print(' ;', end = '')
                currently_awaiting_first_todo_item_note = False
            
            if  args.outtype == 'td':
                print(' ' + m.group(1), end = '')
            elif args.outtype == 'org':
                print(line, end='')
                
            continue

    #    
    # blank line
    #    
    m = re.match(r'^(\s*)$', line)
    if m:
        if previous_line_was_blank_line:
            if  args.outtype == 'td':
                print()
            elif args.outtype == 'org':
                print()

        previous_line_was_blank_line = True
        continue

    #    
    # comment
    #    
    previous_line_was_blank_line = False
    
# the input file has ended; if we are currently in the middle of processing task-associated notes, and we are outputting todo.txt, then we haven't printed the last newline yet
if currently_in_todo_item_notes:
    if args.outtype == 'td':
        print()
