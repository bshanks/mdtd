# section header +project
## subsection header A @context

This is a comment.

- task 1 (implicit project and context)

    comment attached to task 1 separated from task title by empty line,
    and indented by at least 4 spaces. This comment will be separated from the task title with a semicolon.

- task 2 (implicit project and context) @context2

  comment attached to task 2

Another comment, followed by a blank line


---

## subsection header B

Comment: in this subsection project is still implicit (from lvl 1 section header) but context (from previous subsection) is not.

- task 3
    - subtask 3.1
    - subtask 3.2
- task 4
