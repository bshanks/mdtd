# Introduction

mdtd (Markdown Todo) is a subset of Markdown for writing todo lists.

mdtd.py converts an mdtd file into either (1) a todo.txt file, or (2) an org-mode file.

# The mdtd format

The elements of the mdtd format are tasks, task notes, projects, contexts, sections, blank lines, horizontal rules, and comments.

**Tasks** (todo items) are a line that starts with a dash ('-'). Tasks are translated into tasks in todo.txt and into headlines in org-mode. For example:

    - task 1

Tasks can have subtasks, which are like tasks with with 4 more spaces of indentation. Subtasks can have sub-sub-tasks, etc.

Tasks can have associated **task notes**. Notes are separated from the task by a blank line, and are indented at least four spaces further than the tasks they below to. In org-mode, notes are translated into ordinary text/notes, and in todo.txt, notes are appended to the parent task with an intervening ' ; ' to separate the notes from the task title.

    - task 2
    
        Task 2 notes.
        Second line of task 2 notes.

Tasks can have **projects** and **contexts**. Projects are a sequence of non-whitespace characters that start with the character '+'. Contexts are a sequence of non-whitespace characters that start with the character '@'. When translating to todo.txt, projects and contexts translate to themselves. When translating to org-mode, both projects and contexts are translated to tags:

    - task 3 +project @context

The todo list can be structured into hierarchical **sections**. A section is begun by a line starting with (optionally indented) one or more '#' characters, followed by either a newline or a space and then the section header/title text. The more '#' there are, the more nested/'smaller' the section is; that is, you can think of '#' as main sections, '##'s as subsections, '###'s as sub-sub-sections, etc. The maximum number of '#'s is 6. Section headers are translated into headlines in org-mode and tasks in todo.txt. In org-mode, tasks within sections are adjusted to have enough asterisks to be contained within the section, and in todo.txt, tasks within sections are indented to make it clear which section they belong to:

    # section header
    ## subsection header
    
    - this task 4 is contained within the subsection, which is itself contained within the section above

Sections can have projects and contexts. If they do, these projects and contexts are inherited; that is, they apply to every task within the section. When translated to org-mode, this inheritance is automatic, because org-mode itself supports tag inheritance. When translated to todo.txt, mdtd.py adds all inherited projects and contexts to each task:

    # section header +project1 +project2
    ## subsection header @context1 @context2
    
    - this task 5 inherits both projects and both contexts from its containing subsection and section

**Blank lines** are translated to blank lines in both todo.txt and org-mode, but only when there are multiple blank lines in a row; the first blank line in a sequence of blank lines is omitted.

**Horizontal rules** are a line that starts with three or more dashes. They are intended to create a visible boundary within the todo list. In todo.txt, they are translated to a todo item with title '---' (not indented), and in org-mode, they are translated into blank lines (which isn't a great translation but i couldn't find anything better):

    ----

**Comments** are any line that isn't one of the above elements. Comments are omitted from translations of mdtd.


# Example of mdtd format

The following can be found in the file sample.md in this directory.

    # section header +project
    ## subsection header A @context
    
    This is a comment.
    
    - task 1 (implicit project and context)
    
        comment attached to task 1 separated from task title by empty line,
        and indented by at least 4 spaces. This comment will be separated from the task title with a semicolon.
    
    - task 2 (implicit project and context) @context2
    
      comment attached to task 2
    
    Another comment, followed by a blank line
    
    
    ---
    
    ## subsection header B
    
    Comment: in this subsection project is still implicit (from lvl 1 section header) but context (from previous subsection) is not.
    
    - task 3
        - task 3.1
        - task 3.2
    - task 4
    
# Usage of mdtd.py

To translate an mdtd file named FILENAME to todo.txt:

   mdtd.py td FILENAME

To translate an mdtd file named FILENAME to org-mode:

   mdtd.py org FILENAME


